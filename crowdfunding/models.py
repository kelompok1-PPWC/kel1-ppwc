from django.db import models
from django.utils import timezone
from datetime import date
# Create your models here.
class Model_Daftar(models.Model):
	nama = models.CharField(max_length=200)
	tanggal_lahir = models.DateField(max_length=200)
	email = models.EmailField(unique = True)
	password = models.CharField(max_length = 200)
	

class Model_Donasi(models.Model):
	nama_program = models.CharField(max_length=200)
	nama = models.CharField(max_length = 200)
	email = models.EmailField()
	jumlah = models.IntegerField()
	
def __str__(self):
	return self.nama
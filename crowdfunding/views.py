from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Form_Daftar, Form_Donasi
from .models import Model_Daftar, Model_Donasi

response = {}

# Create your views here.

def daftar(request):
	response['form_daftar'] = Form_Daftar
	html = "formdaftar.html"
	response['errmsg'] = ''
	return render(request, html, response)

def savedata(request):
	form = Form_Daftar(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		response['nama'] = request.POST['nama']
		response['tanggal_lahir'] = request.POST['tanggal_lahir']
		response['email'] = request.POST['email']
		response['password'] = request.POST['password']
		project = Model_Daftar(nama = response['nama'], tanggal_lahir = response['tanggal_lahir'], email = response['email'], password = response['password'])
			
		data = Model_Daftar.objects.all().filter(email = response['email']).count()

		if data == 0:
			project.save()
			project = Model_Daftar.objects.all()
			response['project'] = project
			html = 'terdaftar.html'
			return render (request, html, response)
		else:
			response['errmsg'] = 'email sudah terdaftar'
			html = 'formdaftar.html'
			return render (request, html, response)
	else:
		response['form_daftar'] = Form_Daftar
		response['errmsg']='input tidak valid'
		html = "formdaftar.html"
		return render(request, html, response)

def donasi(request):
	response['form_donasi'] = Form_Donasi
	response['errmsg']=''
	html = "formdonasi.html"
	return render(request, html, response)

def savedatadonasi(request):
	form = Form_Donasi(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		response['nama_program'] = request.POST['nama_program']
		response['nama'] = request.POST['nama']
		response['email'] = request.POST['email']
		response['jumlah'] = request.POST['jumlah']
		project = Model_Donasi(nama_program = response['nama_program'], nama = response['nama'], email = response['email'], jumlah = response['jumlah'])
		project.save()
		project = Model_Donasi.objects.all()
		response['project'] = project
		html = 'terdonasi.html'
		response['msg']='sudah donasi'
		return render (request, html, response)
	else:
		response['form_donasi'] = Form_Donasi
		response['errmsg']='input tidak valid'
		html = "formdonasi.html"
		return render(request, html, response)
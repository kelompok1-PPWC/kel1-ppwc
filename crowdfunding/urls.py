from django.urls import path
from . import views

urlpatterns = [
	path('', views.daftar, name = 'daftar'),
	path('daftar', views.daftar, name = 'daftar'),
	path('terdaftar', views.savedata, name = 'terdaftar'),
	path('donasi', views.donasi, name = 'donasi'),
	path('terdonasi', views.savedatadonasi, name = 'terdonasi'),

]
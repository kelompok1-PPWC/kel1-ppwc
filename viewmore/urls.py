from django.urls import path
from .views import index, viewnews, viewprogram

urlpatterns = [
    path('', index, name='index'),
    path('berita', viewnews, name= 'news'),
    path('program', viewprogram, name= 'program')
]
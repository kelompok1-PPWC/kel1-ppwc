from django.shortcuts import render
from django.http import HttpResponse
from .models import Berita, Program

# Create your views here.
def index(request):
    return HttpResponse("Hello Mamank")

def viewnews(request):
	news = Berita.objects.all().values()
	response = {'news': news}
	html = 'htmlview.html'
	return render(request, html, response)

def viewprogram(request):
	progs = Program.objects.all().values()
	response = {'progs': progs}
	html = 'htmlview2.html'
	return render(request, html, response)
	
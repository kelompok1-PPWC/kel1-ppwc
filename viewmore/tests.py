from django.test import TestCase
from django.test import Client
from django.urls import resolve
import unittest
from .models import Program,Berita
from .views import viewnews, viewprogram


# Create your tests here.

class HomeTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/viewmore/berita')
		self.assertEqual(response.status_code, 200)

	def test_function_caller_exist(self):
		found = resolve('/viewmore/berita')
		self.assertEqual(found.func, viewnews)

	def test_if_template_view_is_used(self):
		response = Client().get('/viewmore/berita')
		self.assertTemplateUsed(response, 'htmlview.html')

	def test_url2_is_exist(self):
		response = Client().get('/viewmore/program')
		self.assertEqual(response.status_code, 200)

	def test_function2_caller_exist(self):
		found = resolve('/viewmore/program')
		self.assertEqual(found.func, viewprogram)

	def test_if_template2_view_is_used(self):
		response = Client().get('/viewmore/program')
		self.assertTemplateUsed(response, 'htmlview2.html')
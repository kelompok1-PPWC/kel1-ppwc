# Tugas 1 PPW Kelompok 9

## Anggota Kelompok
1. Naufaldi Athallah Rifqi 1706023782
2. Azhar Difa Arnanda 1706039540
3. Dimas Aditya Faiz 1706039686
4. Faishal Ridwan 1706040201

## Status Aplikasi
[![Pipeline](https://gitlab.com/kelompok1-PPWC/kel1-ppwc/badges/master/pipeline.svg)](https://gitlab.com/kelompok1-PPWC/kel1-ppwc/commits/master)

[![coverage report](https://gitlab.com/kelompok1-PPWC/kel1-ppwc/badges/master/coverage.svg)](https://gitlab.com/kelompok1-PPWC/kel1-ppwc/commits/master)

## Link Heroku App
https://ppw-c-kel1.herokuapp.com/